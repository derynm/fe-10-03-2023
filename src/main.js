import { createApp } from 'vue'
import App from './App.vue'
import CardTes from './components/CardTes.vue'
import './assets/main.css'

// createApp(App).mount('#app')
const app = createApp(App)

app.component('CardTes',CardTes)

app.mount('#app')